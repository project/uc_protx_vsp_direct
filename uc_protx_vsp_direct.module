<?php

/**
 * @file
 * Sage Pay Go with Direct integration payment gateway module for Ubercart.
 *
 * Developed by solarian (http://drupal.org/user/166738).
 * Incorporating suggestions from hanoii (http://drupal.org/user/23157).
 * Extended and updated by longwave (http://drupal.org/user/246492).
 *
 * http://www.sagepay.com/sites/default/files/downloads/DIRECT_Protocol_and_Integration_Guidelines.pdf
 * http://www.sagepay.com/sites/default/files/downloads/SagePaySERVERProtocolandIntegrationGuidelines.pdf
 */

require_once('uc_protx_vsp_direct.ca.inc');

define('UC_PROTX_VSP_DIRECT_DEFAULT_VENDORTXCODE_FORMAT', '[order-id]_[order-uid]_[random-num-9]');


/*******************************************************************************
 * Hook Functions (Drupal)
 ******************************************************************************/

/**
 * Implementation of hook_menu().
 */
function uc_protx_vsp_direct_menu() {
  $items = array();

  $items['cart/checkout/sagepay/3DSecure'] = array(
    'page callback' => 'uc_protx_vsp_direct_3DSecure',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['cart/checkout/sagepay/3DSecure/callback'] = array(
    'page callback' => 'uc_protx_vsp_direct_3DSecure_callback',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['cart/checkout/sagepay/3DSecure/waiting'] = array(
    'page callback' => 'uc_protx_vsp_direct_3DSecure_waitingPage',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implementation of hook_token_list().
 */
function uc_protx_vsp_direct_token_list($type = 'all') {
  if ($type == 'order' || $type == 'ubercart' || $type == 'all') {
    $tokens['order']['sagepay-vendortxcode'] = t('The Sage Pay VendorTXCode associated with the order.');
    $tokens['global']['random-num-4'] = t('A randomly generated four-digit number.');
    $tokens['global']['random-num-9'] = t('A randomly generated nine-digit number.');
  }

  return $tokens;
}

/**
 * Implementation of hook_token_values().
 */
function uc_protx_vsp_direct_token_values($type, $object = NULL) {
  switch ($type) {
    case 'order':
      $comments = uc_order_comments_load($object->order_id, TRUE);
      if (is_array($comments)) {
        foreach ($comments as $comment) {
          $subpatterns = array();
          preg_match('/VendorTxCode: (.*)/', $comment->message, $subpatterns);
          if ($subpatterns[1]) {
            $values['sagepay-vendortxcode']  = $subpatterns[1];
          }
        }
      }
      break;

    case 'global':
      $values['random-num-4'] = mt_rand(1000, 9999);
      $values['random-num-9'] = mt_rand(1000, 9999) . mt_rand(10000, 99999);
      break;
  }

  return $values;
}

/**
 * Implementation of hook_theme().
 */
function uc_protx_vsp_direct_theme() {
  return array(
    'uc_protx_vsp_direct_cards' => array(
      'arguments' => array()
    ),
  );
}

/**
 * Implementation of hook_form_FORM_ID_alter() for uc_payment_methods_form().
 */
function uc_protx_vsp_direct_form_uc_payment_methods_form_alter(&$form, $form_state) {
  $form['method_credit']['cc_fields']['uc_credit_cvv_enabled']['#description'] = t('Required when using Sage Pay.');
  $form['method_credit']['cc_fields']['uc_credit_owner_enabled']['#description'] = t('Optional when using Sage Pay; if disabled, the billing name fields will be used.');
  $form['method_credit']['cc_fields']['uc_credit_start_enabled']['#description'] = t('Recommended when using Sage Pay.');
  $form['method_credit']['cc_fields']['uc_credit_issue_enabled']['#description'] = t('Required when using Sage Pay if you are accepting Maestro or Solo cards.');
  $form['method_credit']['cc_fields']['uc_credit_bank_enabled']['#description'] = t('Not recommended when using Sage Pay.');
  $form['method_credit']['cc_fields']['uc_credit_type_enabled']['#description'] = ' ' . t('Required when using Sage Pay.');
  $form['method_credit']['cc_fields']['uc_credit_accepted_types']['#description'] = t('Enter one card type per line. Accepted values when using Sage Pay: Visa, Visa Delta, Visa Electron, MasterCard, Maestro, Solo, Switch, American Express, Diners Club, JCB.');
  $form['method_credit']['cc_types']['uc_credit_discover']['#description'] = t('When using Sage Pay, enable this to accept Maestro and Solo cards.');
  $form['method_credit']['cc_types']['uc_credit_amex']['#description'] = t('When using Sage Pay, also enable this to accept Diners Club cards.');
}

/*******************************************************************************
 * Hook Functions (Ubercart)
 ******************************************************************************/

/**
 * Implementation of hook_payment_gateway().
 */
function uc_protx_vsp_direct_payment_gateway() {
  $gateways[] = array(
    'id' => 'protx_vsp_direct',
    'title' => t('Sage Pay Direct'),
    'description' => t('Process credit card payments using Sage Pay Go with Direct integration.'),
    'settings' => 'uc_protx_vsp_direct_settings_form',
    'credit' => 'uc_protx_vsp_direct_charge',
    'credit_txn_types' => array(UC_CREDIT_AUTH_CAPTURE, UC_CREDIT_AUTH_ONLY, UC_CREDIT_PRIOR_AUTH_CAPTURE, UC_CREDIT_REFERENCE_CREDIT),
  );

  return $gateways;
}

/**
 * Implementation of hook_store_status().
 */
function uc_protx_vsp_direct_store_status() {
  $vendor = variable_get('uc_protx_vsp_direct_vendor', '');
  if ($vendor) {
    $statuses[] = array(
      'status' => 'ok',
      'title' => t('Sage Pay vendor name'),
      'desc' => t('You have properly set a vendor name for Sage Pay: %vendor.', array('%vendor' => $vendor)),
    );
  }
  else {
    $statuses[] = array(
      'status' => 'error',
      'title' => t('Sage Pay vendor name'),
      'desc' => t('Sage Pay Direct module has not been configured yet. Please configure its settings from the !settings, under the Sage Pay Direct collapsed box.', array('!settings' => l(t('Payment gateways section of Ubercart'), 'admin/store/settings/payment/edit/gateways'))),
    );
  }

  $server = variable_get('uc_protx_vsp_direct_server', 2);
  if ($server < 2) {
    $statuses[] = array(
      'status' => 'warning',
      'title' => t('Sage Pay server'),
      'desc' => t('Sage Pay Direct is not configured to use the Live server. No real transaction will be processed. You can change it in the !settings, under the Sage Pay Direct collapsed box. (Currently set to %server)', array('!settings' => l(t('Payment gateways section of Ubercart'), 'admin/store/settings/payment/edit/gateways'), '%server' => $server == 1 ? t('Test server') : t('VSP Simulator'))),
    );
  }
  else {
    $statuses[] = array(
      'status' => 'ok',
      'title' => t('Sage Pay server'),
      'desc' => t('Sage Pay Direct is configured to use the Live server. Transactions will be processed normally.'),
    );
  }

  return $statuses;
}

/*******************************************************************************
 * Callback Functions, Forms, and Tables
 ******************************************************************************/

/**
 * Callback for payment gateway settings.
 */
function uc_protx_vsp_direct_settings_form() {
  $form['logo'] = array(
    '#value' => theme('image', drupal_get_path('module', 'uc_protx_vsp_direct') .'/img/sagepay-logo.gif', '', '', array('style' => 'float: right;')),
    '#weight' => -20,
  );

  $form['uc_protx_vsp_direct_vendor'] = array(
    '#type' => 'textfield',
    '#title' => t('Sage Pay vendor name'),
    '#default_value' => variable_get('uc_protx_vsp_direct_vendor', ''),
    '#size' => 15,
    '#maxlength' => 15,
  );

  $form['uc_protx_vsp_direct_server'] = array(
    '#type' => 'radios',
    '#title' => t('Sage Pay server'),
    '#options' => array(-1 => t('Showpost (only use when requested by Sage Pay support)'), t('VSP Simulator'), t('Test Server'), t('Live System')),
    '#default_value' => variable_get('uc_protx_vsp_direct_server', 2),
  );

  $form['uc_protx_vsp_direct_iframe'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use inline frames for 3D-Secure'),
    '#default_value' => variable_get('uc_protx_vsp_direct_iframe', 0),
    '#description' => t("<strong>For 3D-Secure transactions only.</strong>  Using an inline frame allows the transaction to appear as if it's all taking place at this store, but it's not W3C standards compliant if you're using the normal (for Drupal) XHTML Strict Document Type Declaration.  However, this is not really a problem in practice, or you can change your theme's DTD (e.g., to a Frameset DTD) if you prefer.  If this setting is turned off, it will be obvious to the user that he is being redirected to a different server to complete the transaction, rather like Sage Pay Form."),
  );

  $form['uc_protx_vsp_direct_vendortxcode_format'] = array(
    '#type' => 'textfield',
    '#title' => t('VendorTXCode format'),
    '#default_value' => variable_get('uc_protx_vsp_direct_vendortxcode_format', UC_PROTX_VSP_DIRECT_DEFAULT_VENDORTXCODE_FORMAT),
    '#description' => t('The VendorTXCode is the unique identifier for a Sage Pay transaction. This field specifies the format of the VendorTXCode using tokens. Multiple transactions may be performed against an order (for example, when an initial attempt at payment fails) so you should include one of the random number tokens to ensure that each generated code is unique. The maximum length of a formatted VendorTXCode is 40 characters.'),
    '#required' => TRUE,
  );

  $form['token_help'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Placeholder tokens'),
    '#description' => t('The following placeholder tokens can be used to generate the VendorTXCode.'),
  );
  $form['token_help']['tokens'] = array(
    '#value' => theme('token_help', 'order'),
  );
  
  $form['cards'] = array(
    '#type' => 'markup',
    '#value' => theme('uc_protx_vsp_direct_cards'),
  );

  return $form;
}

/**
 * Callback for payment processing.
 */
function uc_protx_vsp_direct_charge($order_id, $amount, $data) {
  global $user;

  $order = uc_order_load($order_id);

  // Format a new VendorTXCode using tokens.
  $vendortxcode = variable_get('uc_protx_vsp_direct_vendortxcode_format', UC_PROTX_VSP_DIRECT_DEFAULT_VENDORTXCODE_FORMAT);
  $vendortxcode = token_replace($vendortxcode, 'global');
  $vendortxcode = token_replace($vendortxcode, 'order', $order);
  $vendortxcode = substr($vendortxcode, 0, 40);

  // Fields required for all Sage Pay transactions.
  $transaction = array(
    'VPSProtocol' => '2.23',
    'Vendor' => variable_get('uc_protx_vsp_direct_vendor', ''),
    'VendorTxCode' => $vendortxcode,
    'Amount' => number_format($amount, 2, '.', ''),
    'Currency' => variable_get('uc_currency_code', 'GBP'),
  );

  // Add fields specific to the transaction type.
  switch ($data['txn_type']) {
    case UC_CREDIT_PRIOR_AUTH_CAPTURE:
      // Capture a prior authorisation.
      $transaction = array_merge($transaction, array(
        'TxType' => 'AUTHORISE',
        'Description' => t('Authorising order @orderid.', array('@orderid' => $order_id)),
        'RelatedVPSTxId' => $order->data['sagepay'][$data['auth_id']]['VPSTxId'],
        'RelatedVendorTxCode' => $data['auth_id'],
        'RelatedSecurityKey' => $order->data['sagepay'][$data['auth_id']]['SecurityKey'],
        'ApplyAVSCV2' => '0',
      ));
      break;

    case UC_CREDIT_REFERENCE_CREDIT:
      // Refund a previous transaction.
      $vendortxcode = $data['ref_id'];
      $transaction = array_merge($transaction, array(
        'TxType' => 'REFUND',
        'Description' => t('Refund order @orderid.', array('@orderid' => $order_id)),
        'RelatedVPSTxId' => $order->data['sagepay'][$vendortxcode]['VPSTxId'],
        'RelatedVendorTxCode' => $vendortxcode,
        'RelatedSecurityKey' => $order->data['sagepay'][$vendortxcode]['SecurityKey'],
        'RelatedTxAuthNo' => $order->data['sagepay'][$vendortxcode]['TxAuthNo'],
      ));
      break;

    case UC_CREDIT_AUTH_CAPTURE:
    case UC_CREDIT_AUTH_ONLY:
      $transaction['TxType'] = ($data['txn_type'] == UC_CREDIT_AUTH_CAPTURE) ? 'PAYMENT' : 'AUTHENTICATE';
      // Fall through to default case

    default:
      // Unknown types handled by another module must set TxType in hook_uc_protx_vsp_direct_transaction_alter().

      // Sanity check credit card number, in case validation is disabled.
      if (isset($order->payment_details['cc_number']) && !ctype_digit($order->payment_details['cc_number'])) {
        return array(
          'success' => FALSE,
          'message' => t('You have entered an invalid credit card number.'),
        );
      }

      // CardHolder must be supplied, but use the billing name if the separate field is not available.
      if (!empty($order->payment_details['cc_owner'])) {
        $cardholder = $order->payment_details['cc_owner'];
      }
      else {
        $cardholder = $order->billing_first_name . ' ' . $order->billing_last_name;
      }

      $delivery_country = uc_get_country_data(array('country_id' => $order->delivery_country));
      $billing_country = uc_get_country_data(array('country_id' => $order->billing_country));

      $transaction = array_merge($transaction, array(
        'Description' => _uc_protx_vsp_direct_description($order),
        'CardHolder' => substr($cardholder, 0, 50),
        'CardNumber' => $order->payment_details['cc_number'],
        'ExpiryDate' => sprintf('%02d', $order->payment_details['cc_exp_month']) . substr($order->payment_details['cc_exp_year'], -2),
        'CV2' => $order->payment_details['cc_cvv'],
        'CardType' => _uc_protx_vsp_direct_parse_card_type($order->payment_details['cc_type']),
        'BillingSurname' => substr($order->billing_last_name, 0, 20),
        'BillingFirstnames' => substr($order->billing_first_name, 0, 20),
        'BillingAddress1' => substr($order->billing_street1, 0, 100),
        'BillingAddress2' => substr($order->billing_street2, 0, 100),
        'BillingCity' => substr($order->billing_city, 0, 40),
        'BillingPostCode' => substr($order->billing_postal_code, 0, 10),
        'BillingCountry' => $billing_country[0]['country_iso_code_2'],
        'BillingPhone' => substr(trim(preg_replace('/[^-\d+() ]/', '', $order->billing_phone)), 0, 20),
        'DeliverySurname' => substr($order->delivery_last_name, 0, 20),
        'DeliveryFirstnames' => substr($order->delivery_first_name, 0, 20),
        'DeliveryAddress1' => substr($order->delivery_street1, 0, 100),
        'DeliveryAddress2' => substr($order->delivery_street2, 0, 100),
        'DeliveryCity' => substr($order->delivery_city, 0, 40),
        'DeliveryPostCode' => substr($order->delivery_postal_code, 0, 10),
        'DeliveryCountry' => $delivery_country[0]['country_iso_code_2'],
        'DeliveryPhone' => substr(trim(preg_replace('/[^-\d+() ]/', '', $order->delivery_phone)), 0, 20),
        'Basket' => _uc_protx_vsp_direct_basket($order, $amount),
        'GiftAidPayment' => '0',
        'ApplyAVSCV2' => '0',
        'ClientIPAddress' => ip_address(),
        'Apply3DSecure' => '0',
        'AccountType' => 'E',
      ));

      // Only send email addresses that will validate with Sage Pay.
      if (_uc_protx_vsp_direct_valid_email_address($order->primary_email)) {
        $transaction['CustomerEmail'] = $order->primary_email;
      }

      // US addresses must include the state field.
      if ($billing_country[0]['country_iso_code_3'] == 'USA' && $order->billing_zone) {
        $transaction['BillingState'] = substr(uc_get_zone_code($order->billing_zone), 0, 2);
      }
      if ($delivery_country[0]['country_iso_code_3'] == 'USA' && $order->delivery_zone) {
        $transaction['DeliveryState'] = substr(uc_get_zone_code($order->delivery_zone), 0, 2);
      }

      // If the order is not shippable or the delivery checkout pane is disabled, send billing address as delivery address.
      if (!uc_order_is_shippable($order) || !variable_get('uc_pane_delivery_enabled', TRUE)) {
        foreach ($transaction as $key => $value) {
          if (substr($key, 0, 7) == 'Billing') {
            $transaction['Delivery' . substr($key, 7)] = $value;
          }
        }
      }

      // Send credit card start date if available.
      if (!empty($order->payment_details['cc_start_month']) && !empty($order->payment_details['cc_start_year'])) {
        $transaction['StartDate'] = sprintf('%02d', $order->payment_details['cc_start_month']) . substr($order->payment_details['cc_start_year'], -2);
      }

      // Send issue number for Maestro and Solo cards, if available.
      if (($transaction['CardType'] == 'MAESTRO' || $transaction['CardType'] == 'SOLO') && !empty($order->payment_details['cc_issue'])) {
        $transaction['IssueNumber'] = $order->payment_details['cc_issue'];
      }

      // Allow CA rules to alter the transaction using the helper function.
      $transaction = uc_protx_vsp_direct_txdata_save($transaction);
      ca_pull_trigger('uc_protx_vsp_direct_trigger_txsend', $order, $transaction);
      $transaction = uc_protx_vsp_direct_txdata_save();

      // Allow other modules to alter the transaction.
      drupal_alter('uc_protx_vsp_direct_transaction', $transaction, $order);

      // Don't process the transaction if another module reported a problem.
      if (isset($transaction['Error'])) {
        uc_order_comment_save($order->order_id, $order->uid, $transaction['Error']);
        return array(
          'success' => FALSE,
          'uid' => $user->uid,
          'message' => $transaction['Error'],
        );
      }
      break;
  }

  // Send transaction to the Sage Pay server.
  uc_order_comment_save($order_id, $user->uid, t('Transaction sent.<br />VendorTxCode: @VendorTxCode', array('@VendorTxCode' => $transaction['VendorTxCode'])));

  return uc_protx_vsp_direct_transaction($transaction['TxType'], $transaction, $order, $vendortxcode, $amount);
}

/**
 * Initial 3D-Secure page.
 */
function uc_protx_vsp_direct_3DSecure() {
  // Disable anonymous page cache.
  $GLOBALS['conf']['cache'] = CACHE_DISABLED;

  if (empty($_SESSION['3dsecure']) || !isset($_SESSION['cart_order'])) {
    drupal_set_message(t('Credit card 3D-Secure authorization could not be completed.'), 'error');
    drupal_goto('cart/checkout');
  }

  $term_url = url('cart/checkout/sagepay/3DSecure/callback', array('absolute' => TRUE));

  // Note the different case of "PaReq" here in the HTML element name:
  if (variable_get('uc_protx_vsp_direct_iframe', 0)) {
    drupal_add_css(drupal_get_path('module', 'uc_protx_vsp_direct') .'/uc_protx_vsp_direct.css', 'module', 'all', FALSE);
    drupal_add_js("window.onload = function() { document.forms['uc_protx_vsp_direct_3dsecure'].submit(); }", 'inline');
    $output =
      '<form name="uc_protx_vsp_direct_3dsecure" method="post" action="'. $_SESSION['3dsecure']['ACSURL'] .'" target="Secure3D">
      <p>
      <input type="hidden" name="MD" value="'. $_SESSION['3dsecure']['MD'] .'" />
      <input type="hidden" name="PaReq" value="'. $_SESSION['3dsecure']['PAReq'] .'" />
      <input type="hidden" name="TermUrl" value="'. $term_url .'" />
      </p>
      <noscript>
      <p>
      <input type="submit" value="Click here to continue" />
      </p>
      </noscript>
      </form>
      <iframe name="Secure3D" id="Secure3D" src="'. url('cart/checkout/sagepay/3DSecure/waiting') .'" frameBorder="0">
      </iframe>'
    ;
    return $output;
  }
  else {
    $output =
      '<?xml version="1.0" encoding="utf-8"?>
      <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
      <head>
      <title>3D-Secure</title>
      <script type="text/javascript">window.onload = function() { document.forms[0].submit(); }</script>
      </head>
      <body>
      <form method="post" action="'. $_SESSION['3dsecure']['ACSURL'] .'">
      <p>
      <input type="hidden" name="MD" value="'. $_SESSION['3dsecure']['MD'] .'" />
      <input type="hidden" name="PaReq" value="'. $_SESSION['3dsecure']['PAReq'] .'" />
      <input type="hidden" name="TermUrl" value="'. $term_url .'" />
      </p>
      <noscript>
      <p>
      <input type="submit" value="Click here to continue" />
      </p>
      </noscript>
      </form>
      </body>
      </html>'
    ;
    echo $output;
    exit;
  }
}

/**
 * 3D-Secure result page callback.
 */
function uc_protx_vsp_direct_3DSecure_callback() {
  $order = uc_order_load($_SESSION['cart_order']);

  if (!$order || !isset($_POST['MD']) || !isset($_POST['PaRes']) || !isset($_SESSION['3dsecure']['MD']) || $_POST['MD'] != $_SESSION['3dsecure']['MD']) {
    drupal_set_message(t('Credit card 3D-Secure authorization could not be completed.'), 'error');
    drupal_goto('cart/checkout');
  }

  // Note the different case of "PaRes" in the $_POST version:
  $transaction = array('MD' => $_POST['MD'], 'PARes' => $_POST['PaRes']);
  $result = uc_protx_vsp_direct_transaction('3D-SECURE', $transaction, $order, $_SESSION['3dsecure']['VendorTxCode'], $_SESSION['3dsecure']['amount']);

  unset($_SESSION['3dsecure']);

  if ($result['success']) {
    if ($result['log_payment'] !== FALSE) {
      uc_payment_enter($order->order_id, 'credit', $order->order_total, $order->uid, '', $result['comment']);
    }

    // The redirect to 3D-Secure occured during hook_order('submit').
    // Not all modules may have had chance to act on this hook, so rectify that now.
    // This is required for uc_recurring to process fees at checkout.
    $modules = module_implements('order');
    while ($modules) {
      // 3D-Secure can be invoked from uc_credit or uc_sagepay_token.
      if (array_shift($modules) == 'uc_' . $order->payment_method) {
        break;
      }
    }

    // Continue from the next module in the list.
    // @see uc_cart_checkout_review_form_submit()
    $error = FALSE;
    foreach ($modules as $module) {
      $function = $module .'_order';
      $result = $function('submit', $order, NULL);
      $msg_type = 'status';
      if ($result[0]['pass'] === FALSE) {
        $error = TRUE;
        $msg_type = 'error';
      }
      if (!empty($result[0]['message'])) {
        drupal_set_message($result[0]['message'], $msg_type);
      }

      // Stop invoking the hooks if there was an error.
      if ($error) {
        break;
      }
    }

    // Ignore any error and complete the order anyway, as payment has already been taken.

    $_SESSION['do_complete'] = TRUE;
    unset($_SESSION['sescrd']);
    $redirect = 'cart/checkout/complete';
  }
  else {
    watchdog('uc_payment', 'Payment failed for order @order_id: @message', array('@order_id' => $order->order_id, '@message' => $result['message']), WATCHDOG_WARNING, l(t('view order'), 'admin/store/orders/'. $order->order_id));
    drupal_set_message(variable_get('uc_credit_fail_message', t('We were unable to process your credit card payment. Please verify your card details and try again.  If the problem persists, contact us to complete your order.')), 'error');
    $_SESSION['do_review'] = TRUE;
    $redirect = 'cart/checkout/review';
  }

  if (variable_get('uc_protx_vsp_direct_iframe', 0)) {
    $output =
      '<html><head><title></title></head><body onload="document.forms[0].submit();"><form name="uc_protx_vsp_direct_3dsecure" method="post" action="'. url($redirect) .'" target="_top"></body></html>
      <noscript>
      <input type="submit" value="Please click here to continue." />
      </noscript>
      </form>'
    ;
    echo $output;
    exit;
  }
  else {
    drupal_goto($redirect);
  }
}

/**
 * 3D-Secure iframe waiting page.
 */
function uc_protx_vsp_direct_3DSecure_waitingPage() {
  echo '<html><head><title></title></head><body><p style="font-family: Verdana; color: #AAA; font-weight: bold">Please wait to be redirected to your card issuer for authorization...</p></body></html>';
}


/*******************************************************************************
 * Module and Helper Functions
 ******************************************************************************/

/**
 * Perform a Sage Pay transaction.
 */
function uc_protx_vsp_direct_transaction($method, $transaction, $order = NULL, $vendortxcode = NULL, $amount = NULL) {
  global $user;

  // Send the transaction data to Sage Pay.
  $url = _uc_protx_vsp_direct_url($method);
  $headers = array('Content-Type' => 'application/x-www-form-urlencoded');
  $response = drupal_http_request($url, $headers, 'POST', http_build_query($transaction, '', '&'));
  $result = array('success' => FALSE, 'uid' => $user->uid);

  // Exit immediately if the request failed.
  if ($response->code != 200) {
    $result['message'] = t('Sage Pay HTTP request failed: %error.', array('%error' => $response->code .' '. $response->error));
    if ($order) {
      uc_order_comment_save($order->order_id, $order->uid, $result['message']);
    }
    return $result;
  }

  // Showpost mode returns no response, Sage Pay support must investigate further.
  if (variable_get('uc_protx_vsp_direct_server', 2) == -1) {
    $result['message'] = t('Sage Pay showpost complete. Please contact Sage Pay with your vendor ID and the time and date of this transaction for more information.');
    drupal_set_message($result['message']);
    return $result;
  }

  // Parse response string.
  $data = array();
  $comments = array();
  foreach (explode("\r\n", $response->data) as $str) {
    list($key, $value) = explode('=', $str, 2);
    if ($key) {
      $data[$key] = $value;
      if ($key != 'VPSProtocol') {
        $comments[] = "$key: $value";
      }
    }
  }

  switch ($data['Status']) {
    case '3DAUTH':
      $_SESSION['3dsecure']['ACSURL'] = $data['ACSURL'];
      $_SESSION['3dsecure']['MD'] = $data['MD'];
      $_SESSION['3dsecure']['PAReq'] = $data['PAReq'];

      // Store these in the session, so we have them when returning from 3D Secure.
      $_SESSION['3dsecure']['VendorTxCode'] = $vendortxcode;
      $_SESSION['3dsecure']['amount'] = $amount;
      $_SESSION['3dsecure']['method'] = $method;

      uc_order_comment_save($order->order_id, $user->uid, t('Redirecting to 3D-Secure to authenticate transaction.'));
      drupal_goto('cart/checkout/sagepay/3DSecure');
      break;

    case 'OK':
      $result['success'] = TRUE;

      // If this was the result of a 3D-Secure transaction, the real method is stored in the session.
      if ($method == '3D-SECURE') {
        $method = $_SESSION['3dsecure']['method'];
      }

      switch ($method) {
        case 'AUTHORISE':
          // Log capture of prior authorisation.
          uc_credit_log_prior_auth_capture($order->order_id, $transaction['RelatedVendorTxCode']);
          // Fall through to next case

        case 'PAYMENT':
          // Log transaction reference for capture so it can be refunded later.
          $order->data = uc_credit_log_reference($order->order_id, $vendortxcode, $order->payment_details['cc_number']);
          $order->data['sagepay'][$vendortxcode] = $data;
          db_query("UPDATE {uc_orders} SET data = '%s' WHERE order_id = %d", serialize($order->data), $order->order_id);

          array_unshift($comments, t('Transaction authorized.'));
          break;

        case 'REFUND':
          // Log refund as a negative payment.
          array_unshift($comments, t('Transaction refunded.'));
          uc_payment_enter($order->order_id, 'credit', -$amount, $user->uid, $result['data'], implode($comments, '<br />'));
          $result['log_payment'] = FALSE;
          break;

        default:
          // Another module will be processing this transaction and will need the result data.
          array_unshift($comments, t('Transaction succeeded.'));
          $result['data'] = $data;
      }
      break;

    case 'REGISTERED':
    case 'AUTHENTICATED':
      // Log an authorisation.
      $order->data = uc_credit_log_authorization($order->order_id, $vendortxcode, $amount);
      $order->data['sagepay'][$vendortxcode] = $data;
      db_query("UPDATE {uc_orders} SET data = '%s' WHERE order_id = %d", serialize($order->data), $order->order_id);

      array_unshift($comments, t('Transaction authenticated.'));
      $result['success'] = TRUE;
      $result['log_payment'] = FALSE;
      break;

    case 'REJECTED':
      $result['message'] = t('The VSP System rejected the transaction because of the rules you have set on your Sage Pay account.  The message was: %StatusDetail', array('%StatusDetail' => $data['StatusDetail']));
      break;

    case 'NOTAUTHED':
    case 'INVALID':
    case 'MALFORMED':
    case 'ERROR':
      $result['message'] = $data['StatusDetail'];

      if ($error = uc_protx_vsp_direct_parse_error($data['StatusDetail'])) {
        // Override default credit card error message.
        $GLOBALS['conf']['uc_credit_fail_message'] = $error;
      }
      break;

    default:
      // Check if other modules can handle this response.
      $pass = FALSE;
      foreach (module_implements('uc_protx_vsp_direct_response') as $module) {
        $function = $module . '_uc_protx_vsp_direct_response';
        if ($function($data, $result, $order, $amount, $vendortxcode)) {
          $pass = TRUE;
          break;
        }
      }

      // This should never happen!
      if (!$pass) {
        $result['message'] = t('Sage Pay responded with an unknown status code.');
      }
      break;
  }

  $result['comment'] = implode($comments, '<br />');
  if ($order) {
    uc_order_comment_save($order->order_id, $user->uid, $result['comment']);
  }

  return $result;
}

/**
 * Attempt to parse a Sage Pay error message into something useful for the customer.
 */
function uc_protx_vsp_direct_parse_error($message) {
  switch (intval(substr($message, 0, 4))) {
    case 2000: // Not authorised
      return t('The transaction was not authorised by your bank. Please check your details and try again, or try with a different card.');
    case 2002: // Authorisation timed out
    case 2003: // Server error at Sage Pay
      return t('Your payment could not be processed at the moment. Please try again later.');
    case 3048: // Invalid card length
    case 4021: // Card range not supported
    case 5011: // Card number invalid
      return t('The card number is not valid. Please check and try again.');
    case 3078: // Email invalid
      return t('The email address supplied is not valid. Please check and try again.');
    case 3090: // Postcode blank
    case 5055: // Postcode invalid
      return t('The postal code is not valid. Please check and try again.');
    case 4022: // Card type does not match
      return t('The selected card type does not match the card number. Please check and try again.');
    case 4023: // Issue number invalid
      return t('The card issue number is not valid. Please check and try again.');
    case 4027: // 3D Secure failed
      return t('The Verified by Visa or MasterCard SecureCode password was incorrect. Please try again.');
    case 5036: // Transaction not found
      return t('The transaction timed out. Please try again.');
    case 5038: // Delivery phone invalid
    case 5045: // Billing phone invalid
      return t('The telephone number is not valid. Please check and try again.');
  }

  return FALSE;
}

/**
 * Static cache function for modifying transaction data using Conditional Actions.
 */
function uc_protx_vsp_direct_txdata_save($txdata_new = null) {
  static $txdata = array();

  if (is_array($txdata_new)) {
    $txdata = array_merge($txdata, $txdata_new);
  }

  return $txdata;
}

/**
 * Convert card type string to a Sage Pay CardType identifier.
 */
function _uc_protx_vsp_direct_parse_card_type($type) {
  $searches = array(
    '`.*?delta.*`i' => 'DELTA',// Visa Delta needs to take precedence in this list over ordinary Visa
    '`.*?electron.*`i' => 'UKE', // Visa Electron needs to take precedence in this list over ordinary Visa
    '`.*?visa.*`i' => 'VISA',
    '`.*?master[ ]*card.*`i' => 'MC',
    '`.*?solo.*`i' => 'SOLO',
    '`.*?(maestro|switch).*`i' => 'MAESTRO',
    '`.*?(amex|american[ ]*express).*`i' => 'AMEX',
    '`.*?diner.*`i' => 'DC',
    '`.*?jcb.*`i' => 'JCB',
  );

  foreach ($searches as $pattern => $replace) {
    if (preg_match($pattern, $type)) {
      return $replace;
    }
  }
}

/**
 * Create a Sage Pay "Description" string from an order, based on the most expensive item.
 */
function _uc_protx_vsp_direct_description($order) {
  $description = t('Your order');
  $max = -1;
  foreach ($order->products as $product) {
    if (($product->price * $product->qty) > $max) {
      $description = substr($product->title, 0, 100);
      $max = $product->price * $product->qty;
    }
  }

  if (count($order->products) > 1) {
    $appendix .= ' [etc.] - '. count($order->products) .' products';
    $description = substr($description, 0, 100 - strlen($appendix)) . $appendix;
  }

  return $description;
}

/**
 * Create a Sage Pay "Basket" string from an order.
 */
function _uc_protx_vsp_direct_basket($order, $amount) {
  // The Basket field is <=7,500 characters.
  // The first line is just the total of lines in the basket, followed by a colon.
  // The final line should be shipping, tax, etc., and any items that can't be included in under 7,500 chars.
  // The final line has no final colon
  $basket['strlen'] = 0;
  $basket['subtotal'] = 0;

  // The number of lines of items, inc. the extra one for shipping, tax and any items that don't fit in under 7,500 chars:
  $basket['lines'] = count($order->products) + 1;

  foreach ($order->products as $x => $product) {
    $item_total = $product->price * $product->qty;
    $basket['amounts'][] = $item_total;
    $basket['items'][] = str_replace(
      ':', ' - ', $product->title)
      .':'. $product->qty
      .':' // Net
      .':' // Tax
      .':'. number_format($product->price, 2, '.', '') // Gross
      .':'. number_format($item_total, 2, '.', '')
      .':'
    ;
    $basket['strlen'] += strlen(end($basket['items']));
    $basket['subtotal'] += $item_total;
  }

  $basket['finalLine']['amount'] = $amount - $basket['subtotal'];// At this stage this will probably just be tax & shipping.
  $basket['finalLine']['text'] = '[Other items, shipping and taxes]:::::';

  // The final line has no final colon, but we need one to complete the first line.
  while (  strlen($basket['lines'] .':') + $basket['strlen'] > (7500 - strlen($basket['finalLine']['text'] . $basket['finalLine']['amount']))  ) {
    $basket['strlen'] -= strlen(array_pop($basket['items']));
    $basket['lines']--;
    $basket['finalLine']['amount'] += array_pop($basket['amounts']);
  }

  return $basket['lines'] . ':' . implode($basket['items'], '') . $basket['finalLine']['text'] . $basket['finalLine']['amount'];
}

/**
 * Return a Sage Pay transaction URL for the given transaction type using the current server selection.
 */
function _uc_protx_vsp_direct_url($method) {
  $server = variable_get('uc_protx_vsp_direct_server', 2);

  // Showpost debugging always uses the same URL.
  if ($server == -1) {
    return 'https://test.sagepay.com/showpost/showpost.asp';
  }

  $servers = array(
    'PAYMENT' => array(
      0 => 'https://test.sagepay.com/Simulator/VSPDirectGateway.asp',
      1 => 'https://test.sagepay.com/gateway/service/vspdirect-register.vsp',
      2 => 'https://live.sagepay.com/gateway/service/vspdirect-register.vsp',
    ),
    'AUTHORISE' => array(
      0 => 'https://test.sagepay.com/Simulator/VSPServerGateway.asp?Service=VendorAuthoriseTx',
      1 => 'https://test.sagepay.com/gateway/service/authorise.vsp',
      2 => 'https://live.sagepay.com/gateway/service/authorise.vsp',
    ),
    'REFUND' => array(
      0 => 'https://test.sagepay.com/Simulator/VSPServerGateway.asp?Service=VendorRefundTx',
      1 => 'https://test.sagepay.com/gateway/service/refund.vsp',
      2 => 'https://live.sagepay.com/gateway/service/refund.vsp',
    ),
    '3D-SECURE' => array(
      0 => 'https://test.sagepay.com/Simulator/VSPDirectCallback.asp',
      1 => 'https://test.sagepay.com/gateway/service/direct3dcallback.vsp',
      2 => 'https://live.sagepay.com/gateway/service/direct3dcallback.vsp',
    ),
  );
  $servers['AUTHENTICATE'] = $servers['PAYMENT'];

  drupal_alter('uc_protx_vsp_direct_url', $servers);

  return $servers[$method][$server];
}

/**
 * Copy of drupal valid_email_address() implementation with a twist,
 * Sage Pay want FQDN on the domain part.
 *
 * Verify the syntax of the given e-mail address.
 *
 * Empty e-mail addresses are allowed. See RFC 2822 for details.
 *
 * @param $mail
 *   A string containing an e-mail address.
 * @return
 *   TRUE if the address is in a valid format.
 */
function _uc_protx_vsp_direct_valid_email_address($mail) {
  $user = '[a-zA-Z0-9_\-\.\+\^!#\$%&*+\/\=\?\`\|\{\}~\']+';
  $domain = '(?:(?:[a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])(\..*))+';
  $ipv4 = '[0-9]{1,3}(\.[0-9]{1,3}){3}';
  $ipv6 = '[0-9a-fA-F]{1,4}(\:[0-9a-fA-F]{1,4}){7}';
  return preg_match("/^$user@($domain|(\[($ipv4|$ipv6)\]))$/", $mail);
}

/**
 * Outputs the cards used in the configuration fieldset
 * @return <string>
 */
function theme_uc_protx_vsp_direct_cards() {
  drupal_add_css(drupal_get_path('module', 'uc_protx_vsp_direct') .'/uc_protx_vsp_direct.css', 'module', 'all', FALSE);

  $img_path = base_path() . drupal_get_path('module', 'uc_protx_vsp_direct') .'/img';
  $img_path_cards = base_path() . drupal_get_path('module', 'uc_protx_vsp_direct') .'/img/protx_cards';

  $output = <<<CARDS
<div class="uc_protx_vsp_direct_cards">
<img src="$img_path_cards/mastercard normal.gif" alt="Mastercard" />
<img src="$img_path_cards/visa.gif" alt="Visa" />
<img src="$img_path_cards/delta.gif" alt="Visa Debit" />
<img src="$img_path_cards/electron.gif" alt="Electron" />
<img src="$img_path_cards/amexsmall.gif" alt="American Express" />
</div>
<div class="uc_protx_vsp_direct_cards">
<img src="$img_path_cards/maestro.gif" alt="Maestro" />
<img src="$img_path_cards/solo.gif" alt="Solo" />
<img src="$img_path_cards/dinersclublogo125_26.gif" alt="Diners" />
<img src="$img_path_cards/jcb.gif" alt="JCB" />
</div>
<div class="uc_protx_vsp_direct_cards">
<p>This gateway supports 3D-Secure:</p>
<img src="$img_path/vbv_logo24.gif" alt="Verified by Visa" />
<img src="$img_path/msc_logo24.gif" alt="Mastercard SecureCode" />
</div>
CARDS;

  return $output;
}
