<?php

/**
 * Display the stored credit card terminal page.
 *
 * @see uc_credit_terminal()
 */
function uc_sagepay_token_terminal($order) {
  $output = l(t('Return to order view screen.'), 'admin/store/orders/'. $order->order_id);

  $output .= '<p>'. t('Use this terminal to process payments against stored credit cards.') .'</p>';

  $context = array(
    'revision' => 'formatted-original',
    'type' => 'order_total',
    'subject' => array(
      'order' => $order,
    ),
  );
  $output .= '<div><strong>'. t('Order total: @total', array('@total' => uc_price($order->order_total, $context))) .'</strong></div>'
            .'<div><strong>'. t('Balance: @balance', array('@balance' => uc_price(uc_payment_balance($order), $context))) .'</strong></div>';

  $output .= drupal_get_form('uc_sagepay_token_terminal_form', $order);

  return $output;
}

/**
 * Display the stored credit card terminal form.
 *
 * @see uc_credit_terminal_form()
 */
function uc_sagepay_token_terminal_form($form_state, $order) {
  // Use the credit card terminal form as a base.
  module_load_include('inc', 'uc_credit', 'uc_credit.admin');
  $form = uc_credit_terminal_form($form_state, $order);

  // Replace the credit card selection field with the list of tokens.
  $form['specify_card']['cc_data'] = uc_payment_method_sagepay_token_form(array(), $order);
  $form['specify_card']['cc_data']['#tree'] = TRUE;
  unset($form['specify_card']['cc_data']['cc_policy']);
  unset($form['specify_card']['cc_data']['cc_cvv']['#description']);

  return $form;
}

/**
 * Validate the stored credit card terminal form.
 */
function uc_sagepay_token_terminal_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['amount']) || $form_state['values']['amount'] <= 0) {
    form_set_error('amount', t('You must enter a positive number for the amount.'));
  }
}

/**
 * Submit handler for the stored credit card terminal form.
 *
 * @see uc_credit_terminal_form_submit()
 */
function uc_sagepay_token_terminal_form_submit($form, &$form_state) {
  // Load the order.
  $order = uc_order_load($form_state['values']['order_id']);

  // Get the data from the form and replace masked data from the order.
  $cc_data = $form_state['values']['cc_data'];

  if ($cc_data['cc_cvv'] == str_repeat('-', strlen($cc_data['cc_cvv']))) {
    $cc_data['cc_cvv'] = $order->payment_details['cc_cvv'];
  }

  // Cache the values for use during processing.
  uc_credit_cache('save', $cc_data, FALSE);

  // Build the data array passed on to the payment gateway.
  $data = array();

  switch ($form_state['values']['op']) {
    case t('Charge amount'):
      $data['txn_type'] = UC_CREDIT_AUTH_CAPTURE;
      break;

    case t('Authorize amount only'):
      $data['txn_type'] = UC_CREDIT_AUTH_ONLY;
      break;

    case t('Capture amount to this authorization'):
      $data['txn_type'] = UC_CREDIT_PRIOR_AUTH_CAPTURE;
      $data['auth_id'] = $form_state['values']['select_auth'];
      break;

    case t('Credit amount to this reference'):
      $data['txn_type'] = UC_CREDIT_REFERENCE_CREDIT;
      $data['ref_id'] = $form_state['values']['select_ref'];
  }

  $result = uc_payment_process('credit', $form_state['values']['order_id'], $form_state['values']['amount'], $data, TRUE, NULL, FALSE);

  if ($result) {
    $crypt = new uc_encryption_class;

    // Load up the existing data array.
    $data = db_result(db_query("SELECT data FROM {uc_orders} WHERE order_id = %d", $form_state['values']['order_id']));
    $data = unserialize($data);

    $cache = uc_credit_cache('load');

    $cc_data = array('cc_token' => $cache['cc_token']);

    // Stuff the serialized and encrypted CC details into the array.
    $data['cc_data'] = $crypt->encrypt(uc_credit_encryption_key(), serialize($cc_data));
    uc_store_encryption_errors($crypt, 'uc_credit');

    // Save it again.
    db_query("UPDATE {uc_orders} SET data = '%s' WHERE order_id = %d", serialize($data), $form_state['values']['order_id']);

    drupal_set_message(t('The credit card was processed successfully. See the admin comments for more details.'));
  }
  else {
    if (variable_get('uc_credit_debug', FALSE)) {
      _save_cc_data_to_order(uc_credit_cache('load'), $form_state['values']['order_id']);
    }

    drupal_set_message(t('There was an error processing the credit card.  See the admin comments for details.'), 'error');
  }

  $form_state['redirect'] = 'admin/store/orders/'. $form_state['values']['order_id'];
}

/**
 * Theme function to display a list of saved cards.
 */
function theme_uc_sagepay_token_list_cards($tokens) {
  if (!empty($tokens)) {
    $header = array(
      array('data' => t('Card')),
      array('data' => t('Actions')),
    );
    $rows = array();

    foreach ($tokens as $token) {
      $rows[] = array(
        array('data' => theme('uc_sagepay_token_description', $token)),
        array('data' => l(t('delete'), 'user/' . $token['uid'] . '/delete_card/' . $token['token_id'])),
      );
    }

    return theme('table', $header, $rows);
  }
  else {
    return t('No cards have been saved to your account.');
  }
}

/**
 * Confirmation form to delete a saved card.
 */
function uc_sagepay_token_delete_confirm_form($form_state, $account, $token) {
  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $account->uid,
  );

  $form['token_id'] = array(
    '#type' => 'value',
    '#value' => $token['token_id'],
  );
  
  return confirm_form($form, t('Are you sure you want to delete the saved @card?', array('@card' => theme('uc_sagepay_token_description', $token))), 'user/' . $account->uid, NULL, t('Delete'));
}

/**
 * @see uc_sagepay_token_delete_confirm_form()
 */
function uc_sagepay_token_delete_confirm_form_submit($form, &$form_state) {
  if (uc_sagepay_token_delete($form_state['values']['token_id'], TRUE)) {
    drupal_set_message(t('The card details have been deleted.'));
  }
  else {
    drupal_set_message(t('It was not possible to delete your card details. Please try again later.'));
  }

  $form_state['redirect'] = 'user/' . $form_state['values']['uid'];
}
