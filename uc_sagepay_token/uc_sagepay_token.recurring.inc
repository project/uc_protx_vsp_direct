<?php

/**
 * Implementation of hook_recurring_info().
 */
function uc_sagepay_token_recurring_info() {
  $items['protx_vsp_direct'] = array(
    'name' => t('Sage Pay Direct'),
    'module' => 'uc_sagepay_token',
    'fee handler' => 'sagepay_token',
    'payment method' => 'credit',
    'process callback' => 'uc_sagepay_recurring_process',
    'menu' => array(
      'charge' => UC_RECURRING_MENU_DEFAULT,
      'edit'   => UC_RECURRING_MENU_DEFAULT,
      'cancel' => UC_RECURRING_MENU_DEFAULT,
    ),
  );

  $items['sagepay_token'] = array(
    'name' => t('Sage Pay stored credit card'),
    'module' => 'uc_sagepay_token',
    'fee handler' => 'sagepay_token',
    'payment method' => 'sagepay_token',
    'process callback' => 'uc_sagepay_token_recurring_process',
    'renew callback' => 'uc_sagepay_token_recurring_renew',
    'menu' => array(
      'charge' => UC_RECURRING_MENU_DEFAULT,
      'edit'   => UC_RECURRING_MENU_DEFAULT,
      'cancel' => UC_RECURRING_MENU_DEFAULT,
    ),
  );

  return $items;
}

/**
 * Set up the recurring fee when a new credit card is being stored.
 *
 * Recurring orders are handled by the sagepay_token handler instead.
 */
function uc_sagepay_recurring_process($order, &$fee) {
  $fee->fee_handler = 'sagepay_token';

  return TRUE;
}

/**
 * Set up the recurring fee when a stored credit card is used for the initial payment.
 *
 * The payment data is already set in the original order, so nothing needs to be done.
 */
function uc_sagepay_token_recurring_process($order, &$fee) {
  return TRUE;
}

/**
 * Implementation of hook_recurring_renewal_pending().
 */
function uc_sagepay_token_recurring_renewal_pending(&$order, $fee) {
  // If the original order was made by credit card, switch this order to the
  // token method, using the token that was created against the original.
  if ($fee->fee_handler == 'sagepay_token' && $order->payment_method == 'credit') {
    $token = db_result(db_query("SELECT token_id FROM {uc_sagepay_tokens} WHERE order_id = %d", $order->data['old_order_id']));
    $order->payment_method = 'sagepay_token';
    $order->payment_details['cc_token'] = $token;

    // Clear the credit cache so uc_credit doesn't clobber the token.
    uc_credit_cache('clear');
  }
}

/**
 * Process a renewal.
 */
function uc_sagepay_token_recurring_renew($order, &$fee) {
  $data = array('txn_type' => 'sagepay_token');
  return uc_payment_process('credit', $order->order_id, $order->order_total, $data, TRUE, NULL, FALSE);
}
