<?php

/**
 * Display the Sage Pay PayPal terminal page.
 *
 * @see uc_credit_terminal()
 */
function uc_sagepay_paypal_terminal($order) {
  $output = l(t('Return to order view screen.'), 'admin/store/orders/'. $order->order_id);

  $output .= '<p>'. t('Use this terminal to refund payments against PayPal orders.') .'</p>';

  $context = array(
    'revision' => 'formatted-original',
    'type' => 'order_total',
    'subject' => array(
      'order' => $order,
    ),
  );
  $output .= '<div><strong>'. t('Order total: @total', array('@total' => uc_price($order->order_total, $context))) .'</strong></div>'
            .'<div><strong>'. t('Balance: @balance', array('@balance' => uc_price(uc_payment_balance($order), $context))) .'</strong></div>';

  $output .= drupal_get_form('uc_sagepay_paypal_terminal_form', $order);

  return $output;
}

/**
 * Display the Sage Pay PayPal terminal form.
 *
 * @see uc_credit_terminal_form()
 */
function uc_sagepay_paypal_terminal_form($form_state, $order) {
  $form['order_id'] = array(
    '#type' => 'hidden',
    '#value' => $order->order_id,
  );

  $form['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Refund amount'),
    '#default_value' => 0,
    '#size' => 10,
    '#field_prefix' => variable_get('uc_sign_after_amount', FALSE) ? '' : variable_get('uc_currency_sign', '$'),
    '#field_suffix' => variable_get('uc_sign_after_amount', FALSE) ? variable_get('uc_currency_sign', '$') : '',
  );

  $options = array();
  foreach ((array) $order->data['cc_txns']['references'] as $ref_id => $data) {
    $options[$ref_id] = t('@ref_id - @date', array('@ref_id' => strtoupper($ref_id), '@date' => format_date($data['created'], 'small')));
  }

  if (!empty($options)) {
    $form['references'] = array(
      '#type' => 'fieldset',
      '#title' => t('Customer references'),
      '#description' => t('Use the available buttons in this fieldset to select and act on a customer reference.'),
    );
    $form['references']['select_ref'] = array(
      '#type' => 'radios',
      '#title' => t('Select references'),
      '#options' => $options,
    );
    $form['references']['ref_credit'] = array(
      '#type' => 'submit',
      '#value' => t('Refund amount to this reference'),
    );
  }

  return $form;
}

/**
 * Validate the Sage Pay PayPal terminal form.
 */
function uc_sagepay_paypal_terminal_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['amount']) || $form_state['values']['amount'] <= 0) {
    form_set_error('amount', t('You must enter a positive number for the amount.'));
  }
}

/**
 * Submit handler for the Sage Pay PayPal terminal form.
 *
 * @see uc_credit_terminal_form_submit()
 */
function uc_sagepay_paypal_terminal_form_submit($form, &$form_state) {
  $order = uc_order_load($form_state['values']['order_id']);

  $data = array();
  $data['txn_type'] = UC_CREDIT_REFERENCE_CREDIT;
  $data['ref_id'] = $form_state['values']['select_ref'];

  $result = uc_payment_process('credit', $form_state['values']['order_id'], $form_state['values']['amount'], $data, TRUE, NULL, FALSE);

  if ($result) {
    drupal_set_message(t('The refund was processed successfully.'));
  }
  else {
    drupal_set_message(t('There was an error processing the refund. See the admin comments for details.'), 'error');
  }

  $form_state['redirect'] = 'admin/store/orders/'. $form_state['values']['order_id'];
}
